import numpy as np, pandas as pd, copy, winsound
import matplotlib.pyplot as plt
plt.style.use('ggplot')

'''
Class defined for all search functions that I implement, possible improvements to be made
by considering BFS and beam search.
'''
class searches():
    def __init__(self, features_type, R, phi):
        self.features_type = features_type
        self.phi = phi
        self.R = R
    
    def rgs(self, x, w, num_classes):
        y_vec = []
        best_scores = []

        for i in range(self.R):

            #Make an initial guess
            y_hat = np.random.randint(0, num_classes, size=(len(x)))
            best_score = np.dot(w, self.phi(x, y_hat, num_classes, self.features_type))
            
            #Check neighbohood for high scores and stop when you reach local optima
            flag = False
            while True:

                if flag: break
                for k in range(len(x)):

                    if flag: break
                    for clas in range(num_classes):
                        temp = y_hat[k]
                        y_hat[k] = clas
                        new_score = np.dot(w, self.phi(x, y_hat, num_classes, self.features_type))
                        if (new_score > best_score):
                            best_score = new_score
                        else:
                            y_hat[k] = temp
                            flag = True
                            break

            y_vec.append(y_hat)
            best_scores.append(best_score)
        return y_vec[np.argmax(best_scores)]

'''
The structured learning model in this case is the structured perceptron, the usual implementation
methods have been used.
'''
class structuredPerceptron():
    def __init__(self, R, features_type, eta, MAX, phi, search):
        self.features_type = features_type
        self.eta = eta
        self.MAX = MAX
        self.phi = phi
        self.weights = None
        s = searches(features_type=self.features_type, R=R, phi=self.phi)
        if search == 'rgs':
            self.search = s.rgs

    def fit(self, x, y, num_classes, tit):
        m = len(x[0][0]) 

        #Initialize w to the type of features.
        if self.weights is None:
            if self.features_type == 'pairwise': 
                w = np.zeros(m*num_classes + num_classes**2)
            elif self.features_type == 'triple':
                w = np.zeros(m*num_classes + num_classes**2 + num_classes**3)
            elif self.features_type == 'quadruple':
                w = np.zeros(m*num_classes + num_classes**2 + num_classes**3 + num_classes**4)
        else: w = self.weights

        store = []
        accuracy = 0
        #This loop calls RGS inferance and updtes weights like a perceptron 
        # it executes MAX number of times if not converged over obtained accuracy
        for i in range(self.MAX):
            print('[Iteration ' + str(i+1) + '/' + str(self.MAX) +']')
            total_num = 0
            total_correct = 0
            
            for j in range(len(x)):
                x_j = x[j]
                y_j = y[j]
                y_hat = self.search(x_j, w, num_classes)
                total_num += len(y_j)
                total_correct += np.sum(y_hat == y_j)
                accuracy = (total_correct/total_num)*100

                #If mistake was made then update weights
                if (np.sum(y_hat != y_j) > 0) and (self.weights is None):
                    w = np.add(w, np.dot(self.eta, np.subtract(self.phi(x_j, y_j, num_classes,
                        self.features_type), self.phi(x_j, y_hat, num_classes, self.features_type))))

            print('\tAcc: ' + str(accuracy) + '\n\tNumber of corrects: ' + str(total_correct) 
                + '\n\tTotal Number: ' + str(total_num))
            store.append(accuracy)

            #Check for convergence
            if (i > 0) and (store[-2] > accuracy):
                w = copy.deepcopy(w_prev)
                print('\nReached Local optima..')
                break

            #Store weights so that it can used for the testing if the next iteration converges
            w_prev = copy.deepcopy(w)
        
        plt.title(tit)
        plt.xlabel("Iteration")
        plt.ylabel("Hamming accuracy")
        plt.plot([_+1 for _ in range(len(store))], store)
        plt.savefig(tit+'.png')
        plt.close()

        self.weights = w
    
    def test(self, x, y, num_classes,tit):
        print('Testing!!')
        self.fit(x, y, num_classes, tit)

def feature_function(x, y, num_classes, features_type):
    m = len(x[0]) # length of each letter in a word 81 and 128
    unary_feat = np.zeros(m*num_classes)
    binary_feat = np.zeros(num_classes**2)
    triple_feat = np.zeros(num_classes**3)
    quadruple_feat = np.zeros(num_classes**4)

    #Return the feature function as a concatination of the other.
    for i in range(len(y)):
        unary_feat[m*y[i] : m*(y[i]+1)] += x[i]
    if (features_type == 'pairwise'):
        if (i < len(y) - 1):\
            binary_feat[y[i]*num_classes + y[i+1]] += 1
        return np.concatenate((unary_feat, binary_feat))
    elif (features_type == 'triple'):
        if (i < len(y) - 2):
            triple_feat[y[i]*(num_classes**2) + y[i+1]*num_classes + y[i+2]] += 1
        return np.concatenate((unary_feat, binary_feat, triple_feat))
    elif (features_type == 'quadruple'):
        if (i < len(y) - 3):
            quadruple_feat[y[i]*(num_classes**3) + y[i+1]*(num_classes**2) + y[i+2]*num_classes + y[i+3]] += 1
        return np.concatenate((unary_feat, binary_feat, triple_feat, quadruple_feat))

def parse_data_file(file_loc):
    train_df = pd.read_csv(file_loc, sep='\t', header=None, names=['ind', 'x', 'y', 'delim']).dropna()
    train_df['x'] = train_df['x'].str.replace('im', '')
    alphabet = []
    X = []
    Y = []
    x = []
    y = []

    for i in range(len(train_df)):
        #Stop reading and append word if index of previous is grater than present
        if (i > 0) and (train_df.iloc[i-1]['ind'] > train_df.iloc[i]['ind']):
                X.append(x)
                Y.append(y)
                x = []
                y = []
        temp = []
        for j in train_df.iloc[i]['x']:
            temp.append(int(j))
        x.append(temp)
        try:
            c = int(train_df.iloc[i]['y'])          #Nettalk has integer classes
        except:
            c = ord(train_df.iloc[i]['y']) - 97     #OCR has character classes
        y.append(c)
        if c not in alphabet:
            alphabet.append(c)
    
    return X, Y, alphabet

def main():
    data_dir = "datasets/"
    raw_data = [(data_dir + "nettalk_stress_train.txt", data_dir + "nettalk_stress_test.txt"), 
        (data_dir + "ocr_fold0_sm_train.txt", data_dir + "ocr_fold0_sm_test.txt")]

    for raw_train, raw_test in raw_data:
        for features_type in ['pairwise', 'triple', 'quadruple']:
            #Initialize the class with variables (easier for hyperparameter tuning)
            sp = structuredPerceptron(R=10, features_type=features_type, eta=0.01, MAX=20, 
                phi=feature_function, search='rgs')

            #Call fit function for the current execution, this sets the weights
            x_train, y_train, alphabet = parse_data_file(raw_train)
            tit = raw_train.split('/')[1].split('_')[0].upper() + ' Training ' + features_type + ' features'
            print(tit)
            sp.fit(x_train, y_train, len(alphabet), tit=tit)

            #Use the learned weights for testing
            x_test, y_test, _ = parse_data_file(raw_test)
            tit = raw_train.split('/')[1].split('_')[0].upper() + ' Testing ' + features_type + ' features'
            print(tit)
            sp.test(x_test, y_test, len(alphabet), tit=tit)

            del sp
    
    #To get to know when the code has finally ended
    winsound.Beep(600, 1000)

if __name__ == "__main__": main()
